﻿using System;
using System.Collections.Generic;
using ResearchGroup.BusinessLogic.Entities;

namespace ResearchGroup.Demo
{
    class Program
    {
        private static Group _researchGroup;
        static void Main(string[] args)
        {
            ListProjectInfoByYear(2014);
            AddProject("PR-2", 10000, 2015, "Researcher 3");
            Console.ReadKey();
        }

        private static void ListProjectInfoByYear(int year)
        {
            Console.WriteLine("Listing projects for year " + year);
            var assignments = _researchGroup.GetAssignmentsByYear(year);
            foreach (var assignment in assignments)
            {
                var project = assignment.Project;
                var code = project.Code;
                var import = assignment.Import;
                var mainResearcher = project.MainResearcher.Name;
                Console.WriteLine("Project code:" + code);
                Console.WriteLine("Import: " + import);
                Console.WriteLine("Main researcher: " + mainResearcher);
                Console.WriteLine("Researchers:");
                foreach (var researcher in project.Researchers)
                {
                    var member = researcher.Name;
                    Console.WriteLine("   " + member);
                }
                Console.WriteLine();
            }
        }

        private static void AddProject(string code, double import, int year, string mainResearcherName)
        {
            var annuality = _researchGroup.GetAnnualityByYear(year);
            var mainResearcher = _researchGroup.GetResearcherByName(mainResearcherName);
            var project = new Project(code, mainResearcher);
            var assignment = new Assignment(import, annuality, project);
            annuality.Assignments.Add(assignment);
            project.Assignments.Add(assignment);
            foreach (Researcher researcher in _researchGroup.Researchers)
                project.AddResearcher(researcher);
        }

        private static void initialize()
        {
              var researchers = new List<Researcher>
            {
                new Researcher("Researcher 1"),
                new Researcher("Researcher 2"),
                new Researcher("Researcher 3")
            };
            var annualities = new List<Annuality>
            {
                new Annuality(2014),
                new Annuality(2015)
            };

            var projects = new List<Project>()
            {
                new Project("PR-0", researchers[0]),
                new Project("PR-1", researchers[1])
            };
            researchers[0].MainResearcherProject = projects[0];
            researchers[1].MainResearcherProject = projects[1];

            var assignments = new List<Assignment>
            {
                new Assignment(15000, annualities[0], projects[0]),
                new Assignment(20000, annualities[1], projects[0]),
                new Assignment(9000, annualities[0], projects[1])

            };
            foreach (Assignment assignment in assignments)
            {
                assignment.Annuality.Assignments.Add(assignment);
                assignment.Project.Assignments.Add(assignment);
            }

            foreach (var researcher in researchers)
            {
                projects[0].AddResearcher(researcher);
                projects[1].AddResearcher(researcher);
            }

            _researchGroup = new Group("ISW", researchers, annualities);
        }

    }
}

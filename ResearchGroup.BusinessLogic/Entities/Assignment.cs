﻿namespace ResearchGroup.BusinessLogic.Entities
{
    public sealed class Assignment
	{
	    public Assignment(double import, Annuality annuality, Project project)
	    {
	        Import = import;
	        Annuality = annuality;
	        Project = project;
            //Annuality.Assignments.Add(this);
            //Project.Assignments.Add(this);
	    }

        public readonly double Import;

	    public readonly Annuality Annuality;


	    public readonly Project Project;

	}
}


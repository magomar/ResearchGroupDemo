﻿using System.Collections.Generic;

namespace ResearchGroup.BusinessLogic.Entities
{
    public sealed class Annuality
	{
	    public Annuality(int year)
	    {
	        Year = year;
            Assignments = new List<Assignment>();
	    }

	    public readonly int Year;

        public readonly ICollection<Assignment> Assignments;

	}
}


﻿using System.Collections.Generic;
using System.Linq;

namespace ResearchGroup.BusinessLogic.Entities
{
    public class Group
    {
        public Group(string acronym, ICollection<Researcher> researchers, ICollection<Annuality> annualities)
        {
            Acronym = acronym;
            Researchers = researchers;
            Annualities = annualities;
        }

        public string Acronym
        {
            get;
            set;
        }

        public readonly ICollection<Researcher> Researchers;

        public readonly ICollection<Annuality> Annualities;

        public Annuality GetAnnualityByYear(int year)
        {
            return Annualities.FirstOrDefault(a => a.Year == year);
        }

        public ICollection<Project> GetProjectsByYear(int year)
        {
            var annuality = GetAnnualityByYear(year);
            var projects = new List<Project>();
            if (annuality != null)
                foreach (var assigment in annuality.Assignments)
                {
                    projects.Add(assigment.Project);
                }
            return projects;
        }

        public ICollection<Assignment> GetAssignmentsByYear(int year)
        {
            var annuality = GetAnnualityByYear(year);
            if (annuality != null)
                return annuality.Assignments;
            return new List<Assignment>();
        }

        public Researcher GetResearcherByName(string name)
        {
            return Researchers.FirstOrDefault(r => r.Name == name);
        }
    }
}


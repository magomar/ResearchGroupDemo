﻿using System.Collections.Generic;

namespace ResearchGroup.BusinessLogic.Entities
{
    public sealed class Project
    {
        public Project(string code, Researcher mainResearcher)
        {
            Code = code;
            MainResearcher = mainResearcher;
            Researchers = new List<Researcher>();
            Researchers.Add(MainResearcher);
            Assignments = new List<Assignment>();
        }

        public string Code { get; set; }

        public Researcher MainResearcher { get; set; }

        public readonly ICollection<Assignment> Assignments;

        public readonly ICollection<Researcher> Researchers;

        //private Researcher _mainResearcher;
        //public Researcher MainResearcher
        //{
        //    get { return _mainResearcher; }
        //    set
        //    {
        //        _mainResearcher = value;
        //        value.MainResearcherProject = this;
        //    }
        //}

        public void AddResearcher(Researcher researcher)
        {
            if (!Researchers.Contains(researcher))
            {
                Researchers.Add(researcher);
                researcher.Projects.Add(this);
            }
        }

    }
}


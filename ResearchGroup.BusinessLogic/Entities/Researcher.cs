﻿using System.Collections.Generic;

namespace ResearchGroup.BusinessLogic.Entities
{
    public class Researcher
	{
	    public Researcher(string name)
	    {
	        Name = name;
            Projects = new List<Project>();
	    }

	    public string Name
		{
			get;
			set;
		}

        public readonly ICollection<Project> Projects;

        public Project MainResearcherProject
        {
            get;
            set;
        }

	}
}

